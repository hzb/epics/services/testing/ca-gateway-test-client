FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.0.0 


RUN python3 -m pip install ipykernel==6.29.0
RUN python3 -m pip install ipython==8.12.3
RUN python3 -m pip install jupyter-client==8.6.0
RUN python3 -m pip install jupyter-core==5.7.1
RUN python3 -m pip install numpy==1.24.4
RUN python3 -m pip install ophyd==1.9.0
RUN python3 -m pip install pyepics==3.5.2
RUN python3 -m pip install pytest==7.4.4


RUN git clone --depth 1 --recursive --branch develop https://codebase.helmholtz.cloud/hzb/epics/services/testing/ca-gateway-pytests.git /opt/testing

